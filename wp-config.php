<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'website1_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'n=}bRs*}7F`^WbqodR+SB]n TC#!`10gcEf o#1/rVZO)4f(<1qkc.9yU^76f3:w' );
define( 'SECURE_AUTH_KEY',  'jMq=o^?{<o-Y_#,ap{@`72:y3~2b#<eH7+h2mB#hee(~a2$v20Nyv>Rav4yT}asm' );
define( 'LOGGED_IN_KEY',    'kF{)6XZ#YT/4je6P:9H9moUC @} zvcu?L_^W1i&zfd?TzPNB&Dbq^&Fgx?jBMR/' );
define( 'NONCE_KEY',        '#1*{!2<{6%mo!NE{[=L~cZ|Xnx+CP}nA~nyCQ?pvx]lV<Wan3)<2LE$kl!LZFVb^' );
define( 'AUTH_SALT',        'UtSMnH]KiQ4Z G-oiJLEC_QunZdn/{y.3tw(}(~pTOxHKApB<~Bo>2?<2@yiOkM ' );
define( 'SECURE_AUTH_SALT', '{vI:B$?|/6wPs7F/n1F4H;<6T=S<m<Rpn+tcAF9e}NRPB3#I6/d`9y{Rub8?YORy' );
define( 'LOGGED_IN_SALT',   'pRJ!|_g^GwLo$@?lx[{/F4*M2D@F4P5x0^0^[Qqc !mR+KxdcHNuKpYtw{P|r?H+' );
define( 'NONCE_SALT',       ':z4$<:#J[d~A$w`6uvuh$ej1F^ #gvJ*f$-i1q/AI|Wy!8.@0Qrw&Sjor.nhO4K:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
